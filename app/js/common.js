'use strict';
$(document).ready(function(){

    let $body = $('body'),
    $header = $('header'),
    headerHeight = $header.height();

// Lang swither

    let $langButton = $('#lang-swither');
    let $langList = $('.languages');
    let $currentLang = $langButton.text();

    $langButton.click(function (e) {
        e.preventDefault();

        if( !$langList.hasClass('visible') ) {
            $langList
                .fadeIn(300)
                .css({
                    display: 'flex',
                    flexDirection: 'column'
                });
        }
        if( $langList.hasClass('visible') ) {
            $langList.fadeOut(300);
        }
        $langList.toggleClass('visible');
    });

    $('.languages li').click(function () {
        $langList.fadeOut(300).removeClass('visible');
        $langButton.text($(this).text());
        $(this).text($currentLang);
        $currentLang = $langButton.text();
    });

    $body.mouseup(function (e) {
        e.preventDefault();
        let $tgt = $(e.target);
        if ( !$tgt.closest($langButton, $langList).length ) {
            if($langList.hasClass('visible')) {
                $langList.fadeOut(300).removeClass('visible');
            }
        }
    });

// Top nav

    $('.top_nav li:not(:last-child) a').click(function (e) {
        e.preventDefault();

        $('.top_nav li').removeClass('active');
        $(this).parent().addClass('active');
    });

    var $advLinks = $('.top_nav li:not(:last-child) a');
    let owlAdv = $('#advatages_slider');

    $advLinks.click(function (e) {
        e.preventDefault();
        let index = $advLinks.index( $(this) );

        owlAdv.trigger('to.owl.carousel', index);
    });

// Offers nav

    var $offersLinks = $('.offers_top li a');
    let owl = $('#offers_slider');

    $offersLinks.click(function (e) {
        e.preventDefault();
        let index = $offersLinks.index( $(this) );

        owl.trigger('to.owl.carousel', index);
    });

// Topline toggle class on scroll

    $(window).scroll(function() {
        let top = $(document).scrollTop(),
        screenHeight = window.innerHeight;

        if (top > headerHeight) {
            $('.topline').removeClass('bottom_border');
            $header.addClass("header_on_scroll").addClass('bottom_border');

            if($(window).width() <= 992 && $(window).width() >= 600) {
                $('.mob_menu_btn').css('display', 'flex');
                if( !$('.top_nav').hasClass('active') ) {
                    $('header').css('height', '80px');
                    $('.top_nav').hide();
                }
            }
        }
        if (top < headerHeight) {
            $('.topline').addClass('bottom_border');
            $header.removeClass("header_on_scroll").removeClass('bottom_border');

            if($(window).width() <= 992 && $(window).width() >= 600) {
                $('.mob_menu_btn').hide();
                $('.top_nav').show();
            }
        }
    });
    
// Mob menu

    $('.mob_menu_btn').click(function () {
        if(window.innerWidth >= 600) {
            $('.top_nav').toggle();
            if($('.top_nav').attr('style') == 'display: block;') {
                $('header').css('height', '129px');
                $('.top_nav').addClass('active');
            }
            else{
                $('header').css('height', '80px');
                $('.top_nav').removeClass('active');
            }
        }
        if(window.innerWidth <= 600) {
            $('.mobile_menu, #popup_wrapper').toggle();
            if($('.mobile_menu').attr('style') == 'display: block;') {
                $('#popup_wrapper').css('top', '80px');
                $('body').css('overflow', 'hidden');
                $('.mobile_menu').addClass('active');
            }
            else{
                $('#popup_wrapper').css('top', '0');
                $('body').css('overflow', 'auto');
                $('.mobile_menu').removeClass('active');
            }
        }
    });

    $('.mobile_menu a').each(function () {
        $(this).click(function (e) {
            if( !$(this).hasClass('login') ) {
                $('#popup_wrapper').css('top', '0').hide();
                $('body').css('overflow', 'auto');
                $('.mobile_menu').removeClass('active').hide();
            };
        });
    });

// Body top style

    $body.css({
        marginTop: -headerHeight,
        paddingTop: headerHeight
    });

// Advantages slider

    $('#advatages_slider').owlCarousel({
        loop: false,
        margin: 10,
        navText: [
            '<span><img src="img/svg_icons/prev_slide.svg" alt=""></span>',
            '<span><img src="img/svg_icons/next_slide.svg" alt=""></span>'
        ],
        nav: true,
        autoplay: false,
        dots: false,
        info: true,
        responsive:{
            0:{
                items: 1,
            }
        },
        navContainer: '#advantages_slider_arrows',
        onInitialized: initPaginationAdv,
        onChanged: refreshPaginationAdv,
        onDragged: dragSlideAdv
    });

    function initPaginationAdv(e) {
        $('#active_slide').text( e.item.index + 1);
        $('#slides_count').text( e.item.count );
    }

    function refreshPaginationAdv(e) {
        $('#active_slide').text( e.item.index + 1);
        $('#slides_count').text( e.item.count );
        let liList = [];
        $('.top_nav li').has('a').each(function () {
            liList.push(this);
        });
        let index = e.item.index;
        $('.top_nav li').removeClass('active');
        $(liList[index]).addClass('active');
    }

    function dragSlideAdv(e) {
        $('#active_slide').text( e.item.index + 1);
        $('#slides_count').text( e.item.count );
        let liList = [];
        $('.top_nav li').has('a').each(function () {
            liList.push(this);
        });
        let index = e.item.index;
        $('.top_nav li').removeClass('active');
        $(liList[index]).addClass('active');
    }


// Offers slider

    $('#offers_slider').owlCarousel({
        loop: false,
        margin: 40,
        navText: [
            '<span><img src="img/svg_icons/prev_slide.svg" alt=""></span>',
            '<span><img src="img/svg_icons/next_slide.svg" alt=""></span>'
        ],
        nav: true,
        autoplay: false,
        dots: false,
        info: true,
        responsive:{
            0:{
                items: 1,
            }
        },
        navContainer: '#offers_slider_arrows',
        onChanged: refreshPagination,
        onDragged: dragSlide
    });

    function refreshPagination(e) {
        let liList = [];
        $('.offers_top li').each(function () {
            liList.push(this);
        });
        let index = e.item.index;
        $offersLinks.removeClass('active');
        $(liList[index]).find('a').addClass('active');
    }

    function dragSlide(e) {
        let liList = [];
        $('.offers_top li').each(function () {
            liList.push(this);
        });
        let index = e.item.index;
        $offersLinks.removeClass('active');
        $(liList[index]).find('a').addClass('active');
    }

// Case slider

    $('#we_done_slider').owlCarousel({
        loop: false,
        stagePadding: window.innerWidth > 1400 ? 300 : 150,
        margin: 32,
        center: true,
        navText: [
            '<span><img src="img/svg_icons/prev_slide.svg" alt=""></span>',
            '<span><img src="img/svg_icons/next_slide.svg" alt=""></span>'
        ],
        nav: true,
        autoplay: false,
        dots: false,
        info: true,
        items: 1,
        responsive:{
            0:{
                stagePadding: 0,
                margin: 50

            },
            768:{
                stagePadding: 38
            },
            800:{
                stagePadding: 50
            },
            992: {
                stagePadding: 150
            },
            1400: {
                stagePadding: 300
            }
        },
        navContainer: '#we_done_slider_arrows',
    });

// Case popup slider

    let activeCasePopupSlide = localStorage.getItem('activeCasePopupSlide') ? +localStorage.getItem('activeCasePopupSlide') : 0;

function initCasePopupSlider(i)  {

    $('#case_popup_slider').owlCarousel({
        loop: false,
        margin: 32,
        navText: [
            '<span><img src="img/svg_icons/prev_slide.svg" alt=""></span>',
            '<span><img src="img/svg_icons/next_slide.svg" alt=""></span>'
        ],
        nav: true,
        autoplay: false,
        dots: false,
        info: true,
        items: 1,
        responsive:{
            0:{
                stagePadding: 0,
                margin: 50
            },
            768:{
                stagePadding: 38
            },
            800:{
                stagePadding: 50
            },
            992: {
                stagePadding: 150
            },
            1400: {
                stagePadding: 300
            }
        },
        navContainer: '#case_popup_slider_arrows',
        onChanged: setActiveSlide,
    });

    function setActiveSlide(e) {
        localStorage.setItem('activeCasePopupSlide', e.item.index)
    }

    let owl = $('#case_popup_slider');

    owl.trigger('to.owl.carousel', i);

}

    initCasePopupSlider(activeCasePopupSlide);

// VideoBtnPulse

    let pulsingBtn = $(".video_btn_inner");
    startAnimation();
    function startAnimation(){
        pulsingBtn.animate({height: 76, width: 76}, 600);
        pulsingBtn.animate({height: 65, width: 65}, 600, startAnimation);
    }

// Partners

/*    $('.partner').each(function () {
        $(this).hover(function () {
            $(this).find('img.mono').fadeOut(0);
            $(this).find('img.color').fadeIn(300, 'swing');
        }, function () {
            $(this).find('img.color').fadeOut(0);
            $(this).find('img.mono').fadeIn(300, 'swing');
        })
    });*/

    $('.partners_top ul a').each(function () {
       $(this).click(function (e) {
           e.preventDefault();
           var linkText = $(this).text();

           $('.partners_top ul a').removeClass('active');
           $(this).addClass('active');
           $('.partner').fadeOut(0);

           if( linkText != 'All' ) {
               $('.partner').each(function () {
                   if( $(this).attr('data-partner-type') == linkText ) {
                       $(this).fadeIn();
                   }
               })
           }
           else  {
               $('.partner').fadeIn();
           }

       })
    });

// FAQ

    let $faqLinks = $('.faq_quetions li a');
    let $faqAnswers = $('.answer');
    let answersArr = [];

    $faqAnswers.each(function () {
        answersArr.push($(this));
    });
    $faqAnswers.fadeOut(0);
    $(answersArr[0]).fadeIn();

    $faqLinks.each(function () {
        $(this).click(function (e) {
            e.preventDefault();
            let index = $faqLinks.index($(this));

            $faqAnswers.fadeOut(0);
            $(answersArr[index]).fadeIn(500);
        });
    });

// Datepicker

    $('#date_callback_popup').datetimepicker({
        format:'d.m.Y H:i',
        inline:false,
        lang:'ru',
    });

    $('#date_callback_popup').click(function (e) {
        e.preventDefault();
    });

// Callback popup dropdowns and file input

    $('#timezone_callback_popup').parent().click(function (e) {
        e.preventDefault();
        if ( !e.target.closest('.timezone_dropdown') ) {
            $('#timezone_callback_popup').blur();
            $('.timezone_dropdown').fadeIn(300).css('display', 'block');
            $('.timezone_dropdown').each(function () {
                new SimpleBar($(this)[0]);
            });
        }
    });

    $('.timezone_dropdown li').click(function () {
        $('#timezone_callback_popup').val($(this).text());
        $('.timezone_dropdown').fadeOut(300);
    });

    $body.mouseup(function (e) {
        e.preventDefault();
        let $tgt = $(e.target);
        if ( !$tgt.closest('.timezone_dropdown').length ) {
            $('.timezone_dropdown').fadeOut(300);
        }
    });

    $('#callback_by_callback_popup').parent().click(function (e) {
        e.preventDefault();
        if( !e.target.closest('.contact_by_dropdown') ) {
            $('#callback_by_callback_popup').blur();
            $('.contact_by_dropdown').fadeIn(300).css('display', 'flex');
        }
    });

    $('.contact_by_dropdown li').click(function () {
        $('#callback_by_callback_popup').val($(this).text());
        $('.contact_by_dropdown').fadeOut(300);
    });

    $body.mouseup(function (e) {
        e.preventDefault();
        let $tgt = $(e.target);
        if ( !$tgt.closest('.timezone_dropdown').length ) {
            $('.contact_by_dropdown').fadeOut(300);
        }
    });

    $('input[type="file"]').change(function(e) {
        if(e.target.files[0]) {
            var fileName = e.target.files[0].name;
            $(this).parent().find('span').text(fileName);
        }
        else {
            $(this).parent().find('span').text('Upload file');
        }
    });

// Login/Registration popup

    $('a.login').click(function (e) {
        e.preventDefault();
        $('#team_popup_wrapper').show();
        $('.login_registration_popup').show();
    });

    $('#team_popup_wrapper').mouseup(function (e) {
        e.preventDefault();
        $('#team_popup_wrapper').hide();
        $('.login_registration_popup').hide();
    });

    $('.login_registration_popup_nav div a').each(function () {
       $(this).click(function (e) {
           e.preventDefault();
           $('.login_registration_popup_content div, .login_registration_popup_nav div').removeClass('active');
           let activeClass = $(this).parent()[0].className;
           $('.login_registration_popup_content div, .login_registration_popup_nav div').each(function () {
               if( $(this).hasClass(activeClass) )
                   $(this).addClass('active');
           })
       })
    });

    $('#user_type_register').click(function (e) {
        e.preventDefault();
        $(this).blur();
        $('.user_type_dropdown').fadeIn(300).css('display', 'flex');
    });

    $('.user_type_dropdown li').click(function () {
        $('#user_type_register').val($(this).text());
        $('.user_type_dropdown').fadeOut(300);
    });

    $body.mouseup(function (e) {
        e.preventDefault();
        let $tgt = $(e.target);
        if ( !$tgt.closest('.user_type_dropdown').length ) {
            $('.user_type_dropdown').fadeOut(300);
        }
    });

    $('a.show_password').each(function () {
       $(this).click(function (e) {
           e.preventDefault();
           if( $(this).parent().find('input').attr('type') == 'password' ) {
               $(this).parent().find('input').attr('type', 'text');
               $(this).find('svg').find('path').css('fill', '#31DEE0');
           }
           else {
               $(this).parent().find('input').attr('type', 'password');
               $(this).find('svg').find('path').css('fill', '#393939');
           }
       });
    });

// Callback popup

    $('.contact_us a.callback').click(function (e) {
        e.preventDefault();

        $('#popup_wrapper').show();
        $('.callback_popup').show();
        $("body").css("overflow","hidden");
        $($header).hide();
    });

    $('.callback_popup .popup_close').mouseup(function (e) {
        e.preventDefault();

        $('#popup_wrapper').hide();
        $("body").css("overflow","auto");
        $('.callback_popup').hide();
        $($header).show();
    });

// Team popup

    $('.team-member a.read-more').each(function () {
       $(this).click(function (e) {
           e.preventDefault();
           let member = $(this).parent(),
               popup = $('.team_popup'),
               popupLeft = $('.team_popup_left'),
               popupRight = $('.team_popup_right');

           $(popupLeft).append( $(member).find('.member-info').clone() );
           $(popupRight).append( $(member).find('.member-description').clone().removeClass('small_text') );
           $('#team_popup_wrapper').show();
           $(popup).show().css('display', 'flex');
       });
    });

    $('.team_popup .popup_close').mouseup(function (e) {
        e.preventDefault();
        $('#team_popup_wrapper').hide();
        $('.team_popup').hide();
        $('.team_popup_left, .team_popup_right').empty();
    });

    $('#team_popup_wrapper').mouseup(function (e) {
        e.preventDefault();
        $('#team_popup_wrapper').hide();
        $('.team_popup').hide();
        $('.team_popup_left, .team_popup_right').empty();
    });

// Case popup

    /*$('#we_done_slider a.read-more').each(function () {
        $(this).click(function (e) {
            e.preventDefault();
            let index = $('#we_done_slider a.read-more').index($(this)),
                owl = $('#case_popup_slider');

            owl.trigger('to.owl.carousel', index);
            $('#popup_wrapper').show();
            $('.case_popup').show();
            $("body").css("overflow","hidden");
        });
    });

    $('.case_popup_top').mouseup(function (e) {
        e.preventDefault();
        $('#popup_wrapper').hide();
        $('.case_popup').hide();
        $("body").css("overflow","auto");
    });*/

    /*$('.case_video a').each(function () {
        $(this).click(function (e) {
            e.preventDefault();

            $(this).hide();
            $(this).parent().find('.video_wrapper').show();
        });
    });*/

    $('#we_done_slider a.read-more').each(function () {
        $(this).click(function (e) {
            e.preventDefault();
            let index = $('#we_done_slider a.read-more').index($(this));

            localStorage.setItem('activeCasePopupSlide', index);

            $(location).attr('href', 'cases.html');
        });
    });

// Contact us popup

    $('.case_popup a.read-more').each(function () {
        $(this).click(function (e) {
            e.preventDefault();
            $('#popup_wrapper').show();
            $('.contact_us_popup').show();
            $("body").css("overflow","hidden");
            $($header).hide();
        });
    });

    $('.contact_us_popup .popup_close').mouseup(function (e) {
        e.preventDefault();
        $('.contact_us_popup').hide();
        $('#popup_wrapper').hide();
        $("body").css("overflow","auto");
        $($header).show();
    });

    $('.contact_us_popup a.callback').each(function () {
        $(this).click(function (e) {
            e.preventDefault();
            $('.contact_us_popup').hide();
            $('.callback_popup').show();
            $($header).hide();
        });
    });

// Page nav

    $(".top_nav, .footer_nav, .mobile_menu").on("click","a[href^='#']", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top - $('header').innerHeight() - 10}, 1000);
    });

// Resize events
    $(window).on('resize', function() {
        if(window.innerWidth <= 992 && $('header').hasClass('header_on_scroll')) {
            $('.mob_menu_btn').show();
        }
        if(window.innerWidth <= 600) {
            $('.mob_menu_btn').show();
        }
        else {
            $('.mob_menu_btn').hide()
        }
    });

// Scrolls

    $('.slide-content .small_text').each(function () {
        new SimpleBar($(this)[0]);
    });

    $('.login_registration_popup').each(function () {
        new SimpleBar($(this)[0]);
    });

    $('.callback_popup').each(function () {
        new SimpleBar($(this)[0]);
    });

    $('.case_popup').each(function () {
        new SimpleBar($(this)[0]);
    });

    $('.contact_us_popup').each(function () {
        new SimpleBar($(this)[0]);
    });

    $('.team_popup').each(function () {
        new SimpleBar($(this)[0]);
    });
});